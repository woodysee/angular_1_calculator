(
    () => {
        angular.module("calculator").controller("CalcVM", CalcVM);

        function CalcVM() {
            var calc = this;
            //Defines what is shown on the calculator display on user interaction
            calc.display = "";

            //Appends a symbol (either a digit or operator) to the calculator's display
            calc.addSymbol = (symbol) => {
              calc.display += symbol;
            };
            //Takes calc.display represented as a string and returns its results. Source: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/eval
            calc.ulate = () => {
              calc.display = eval(calc.display).toString();
            };
        }
    }
)();