(
  () => {
    angular.module("calculator").controller("SwitchVM", SwitchVM);

    function SwitchVM() {
      var vm = this;

      //
      vm.routes = {
        HOME: "HOME",
        CALCULATOR: "CALCULATOR"
      };

      //Default route so we will see this view initially.
      vm.route = vm.routes.HOME;

      //Toggle route visibility as a function which returns boolean values that are set for each `.route` directed by `ng-show`.
      vm.showRoute = (route) => {
        return vm.route == route;
      };

      //Opens the route as a function describing which `.route` to show and opens that `.route` when invoked.
      vm.open = (route) => {
        vm.route = route;
      };
    }
  }
)();
