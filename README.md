## This is a calculator app made using Angular 1.x. ##

### Setting up ###

* Set up the node.js back-end web app `server/server.js`:
    - $ `git init`
    - $ `git remote add origin <git url>`
    - $ `npm init`
    (Answer all the questions , enter the entry point accordingly)
    - $ `mkdir <server directory>`
    - Create an `/app.js` under the server directory
    - Create `.gitignore` to ignore directories/files, e.g. `node_modules` and `client/bower_components`

* Create a `.bowerrc` with the contents:

        {
            "directory": 
            "client/bower_components"
        }

* $ `bower init`

    - Set currently installed components as dependencies? **Yes**
    - Add commonly ignored files to ignore list? **Yes**
    - Would you like to mark this package as private which prevents it from being accidentally published to the registry? **No**

* $ `bower install bootstrap font-awesome angular --save`

## Construction ##

### In `client/` ... ###
1. Make the following empty directories:
    - `/views/` where we will store our views, except `/index.html`.
2. Touch the following empty files:
    - `/index.html` - the Entry View on load.
        - Inside `<head>` before `<body>`, in order from most to least dependent, declare `<link>` dependencies:
            1. External style dependencies with Bower, e.g. `path/to/css/bootstrap.min.css`,`path/to/css/bootstrap-theme.min.css`, `path/to/font-awesome/css/font-awesome.min.css`
            2. Locally created style dependencies, e.g. `/client/calculator.css`
        - After `<body>` content, in order from most to least dependent, declare `<link>` dependencies:
            1. External script dependencies with Bower:
				- jQuery: `path/to/jquery/dist/jquery.min.js`
				- Angular: `path/to/bower_components/angular/angular.min.js`
				- Bootstrap: `path/to/bower_components/bootstrap/dist/js/bootstrap.min.js`
            2. Locally created script dependencies:
                - Entry point: `client.module.js`
                - Controllers / Viewmodels: `switch.controller.js`,`calculator.controller.js`
    - `/calculator.css` - the main stylesheet for the entry view of the Angular calculator
    - `/client.module.js` - the entry Angular module
    - `/switch.controller.js` - the entry viewmodel & frontend router
3. To set up the entry app `calculator` using MVVM architecture principles:
    - In `/index.html`, within the opening `<html>` tag, describe a `ng-app="calculator"` directive declaring `calculator` which points to the `/client.module.js`.
    - In `/client.module.js`, declare an Angular `calculator` app with `angular.module()` with no dependency injections.
    1. **FRONTEND NAKED SWITCH - VIEW**
        - Touch 2 views:
            - a cover page (`views/home.html`), &
            - a calculator (`views/calculator.html`).
        - Each view needs to be toggled through the entry point `/index.html` on clicking buttons controlled by a naked **frontend switch**.
        - In `/index.html`, we describe a switch:
            - **Switch**
                - [`ng-controller`](https://docs.angularjs.org/api/ng/directive/ngController) directs `#switch` from `SwitchVM` constructed in `/switch.controller.js` imported within `/index.html`.

            -  **Route**
				- [`ng-include`](https://docs.angularjs.org/api/ng/directive/ngInclude) directs the directed `.route` to the file via the path.
                - [`ng-show`](https://docs.angularjs.org/api/ng/directive/ngShow) directs the directed `.route` by toggling the element's visibility.
        
        ```
        <!-- Router -->
        <div id="switch" ng-controller="SwitchVM as switch">
            <!-- Example route -->
            <div class="route" ng-include=" 'exact/path/to/your.html' " ng-show="switch.showRoute(switch.routes.YOUR)">
            </div>
            <!-- More routes -->
        </div>
        ```

        - [What is the difference between `ng-show` / `data-ng-show` directives](https://docs.angularjs.org/guide/directive)? According to the Angular 1.x team, the "following forms are all equivalent".
        - As hinted in the title, do not use `ng-include` to conceal sensitive information: as encapsulated content is not truly hidden from the client.

    2. **FRONTEND NAKED SWITCH - VIEWMODEL**
        - In `/switch.controller.js`, we will implement the `$controller` service to construct the entry controller `switchVM`:
            - "switchVM" is described to `/index.html` through the locally created script dependency and directed through `ng-controller="switchVM"`.
            - `switchVM` points to the controller constructor being defined.
        - Taking a [leaf](https://johnpapa.net/angularjss-controller-as-and-the-vm-variable/) out of John Papa's proverbial book, use `vm` when we refer to `this` within both the directive's or controller's scope. As the number of viewmodels increase, using specific descriptors as names, e.g. `switch`, within the scope allows us to better distinguish different directive/controller scopes.
        - Within the controller scope, we declare:
            a. **Routes** as `vm.routes`

            ```
            vm.routes = {
                HOME: "HOME",
                CALCULATOR: "CALCULATOR",
                YOUR: "YOUR",
                SOME_ROUTE: "SOME_ROUTE"
            };
            ```

            b. **Default route** as `vm.route = vm.routes.HOME` so we will see this view initially.
            c. **Toggle route visibility** as a function `vm.showRoute = (route) => {return vm.route == route}` which returns boolean values that are set for each `.route` directed by `ng-show`.
            d. **Open route** as a function `vm.open = (route) => {vm.route = route}` describing which `.route` to show and opens that `.route` when invoked.

4. To set up the HOME component,
    - Describe `/home.html`:
        -**Proceed button** described as a `<button>` directed by `ng-click` invoking `switch.open(switch.routes.CALCULATOR)` which will open the calculator component.
5. To set up the CALCULATOR component,
    1. **CALCULATOR - VIEWS**
		- Describe `/calculator.html`:
			- **Calculator display** described as a `<textarea>` directed by `ng-model` which only prints `calc.display` from `CalcVM` on user interaction.
			- **Calculator keys** described as `<button>`s directed by `ng-click` invoking `calc.addSymbol('%')` into `CalcVM` pointing to `calc.display` through the `ng-model` directive.
			- **Equal key** described as a `<button>` directed by `ng-click` invoking `calc.ulate()` into `CalcVM` to mutilate `calc.display`.
			- **Back button** described as a `<button>` directed by `ng-click` invoking `switch.open(switch.routes.HOME)` which will go back to the cover page.
		- Describe a `ng-controller` directive in `#calculator` importing `CalcVM` as `calc` from `/calc.controller.js`.
    2. **CALCULATOR - VIEWMODEL**
		- In `/calc.controller.js` viewmodel, we will implement the `$controller` service to construct the calculator controller `CalcVM`:
			- "CalcVM" is described to `views/calculator.html` through the locally created script dependency and directed through `ng-controller="CalcVM"`.
			- `CalcVM` points to the controller constructor being defined.
		- Within the controller scope, we declare:
			1. **Calculator display** as a string, `calc.display`, which defines what is shown on the calculator display `calc.display` on user interaction
			2. **On click of any calculator key (except equal key)** as a function, `calc.addSymbol(symbol)`, which appends the value of the `symbol` parameter (either a digit or operator) to the calculator display `calc.display` string.
			3. **On click of equal key** as a function `calc.ulate()` which evaluates current `calc.display` string value and sets calculator display `calc.display` as its results.

5. You can start this app by running $`nodemon` in the project folder.

## References ##
https://bitbucket.org/iss_startup/day-07-exercise-02
Original calculator: https://bitbucket.org/iss_startup/fsf2017r01-day07-exer-calculator-solution