const express = require("express");
const bodyParser = require("body-parser");

const NODE_PORT = process.env.NODE_PORT || 3001;

const server = express();

server.use(
  bodyParser.urlencoded(
    { extended: false }
  )
);
server.use(
  bodyParser.json()
);

server.use(
  express.static(
    __dirname + "/../client/"
  )
);

server.listen(
  NODE_PORT,
  () => {
    console.log("server/server.js: express server is now running locally on port: " + NODE_PORT);
  }
);